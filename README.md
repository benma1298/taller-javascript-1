
Taller de Programacion Web
Desarrollo en JavaScript

Estudiante: Benjamin Martin Albornoz.
Fecha: 16 de Mayo del 2023.

Se pide desarrollar un sitio web del tipo Onepage que contenga el desarrollo de los siguientes ejercicios:

1. Crear una calculadora básica que realice las siguientes operaciones 
    a. Sumar 
    b. Restar 
    c. Multiplicar 
    d. Dividir 
    e. Porcentaje 
    f. Limpiar memoria 

2. Crear  un  juego  de  adivinanza  en  el  que  el  usuario  tiene  que  adivinar  un  número generado aleatoriamente. El juego debe proporcionar pistas si el número introducido es demasiado alto o bajo.

3. Crear el juego de la lotería. El jugador debe ingresar seis números y se debe verificar si es el ganador o no. En ambos casos se deben mostrar los números sorteados e indicar cuales de los números ingresados salieron sorteados.

4. Crear una lista de tareas por hacer. Las funcionalidades que debe tener la lista son: 
    a. Añadir un elemento a la lista. 
    b. Marcar como completado un elemento. 
    c. Eliminar un elemento de la lista.

5. Crear un calculador del digito verificador del RUN. El usuario debe ingresar el rut sin el digito y se debe mostrar en pantalla cual es el digito calculado. 

Para la elaboracion de este Taller, se realizo un codigo en HTML llamado "tallerjs.html", ademas de un codigo en css llamado "style.css"; para el caso de los codigos en JavaScript, se realizo 1 por cada item solicitado, con los nombres de:
        1.- "calculadora.js"
        2.- "adivinanza.js"
        3.- "loteria.js"
        4.- "lista.js"
        5.- "verificador.js"

Para la ejecucion de esta Pagina web, se necesita tener todos los archivos del repositorio gitlab en una misma carpeta, y ejecutar el archivo denominado "tallerjs.html"

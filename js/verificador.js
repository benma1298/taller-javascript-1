// Funcion para calcular el digito verificador
function calcularDigitoVerificador() {

  // Se obtiene el valor del RUN ingresado por el usuario 
  var rut = document.getElementById('rutInput').value;
  // Se eliminan los puntos (.) y guiones (-) del RUN 
  rut = rut.replace(/\./g, '').replace('-', '');
  // Se convierte el RUN en un array se revierte su orden
  var rutArray = rut.split('').reverse();

  // Se declaran las variables para el calculo de la suma ponderada
  var multiplicador = 2;
  var suma = 0;

  // Se recorre cada digito del RUN y se calcula la suma ponderada
  for (var i = 0; i < rutArray.length; i++) {
    suma += parseInt(rutArray[i]) * multiplicador;

    // Se incremena el multiplicador, que parte en 2 y se reinicia si alcanza el valor de 7
    multiplicador++;
    if (multiplicador > 7) {
      multiplicador = 2;
    }
  }

  // Se calcula el resto de la suma dividida por 11
  var resto = suma % 11;
  // Se calcula el digito verificador restanto el "resto" a 11
  var digitoVerificador = 11 - resto;

  // Si el digito verificador es 11, se asigna un 0; en el caso de ser 10, se asigna una K
  if (digitoVerificador === 11) {
    digitoVerificador = 0;
  } else if (digitoVerificador === 10) {
    digitoVerificador = 'K';
  }

  // Se actualiza el contenido del elemento HTML con el resultado del calculo
  document.getElementById('resultado').textContent = 'Digito verificador: ' + digitoVerificador;
}

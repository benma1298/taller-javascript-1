// Se declaran las variables
const display = document.querySelector(".display-calculadora");
const buttons = document.querySelectorAll(".buttons button");
const specialChars = ["%", "*", "/", "-", "+", "="];
let output = "";

// Se define la funcion calcular en funcion del boton seleccionado
const calculate = (btnValue) => {
  display.focus();
  if (btnValue === "=" && output !== "") {
    // Si la salida posee '%' se reemplaza por '/100' antes de evaluar
    output = eval(output.replace("%", "/100"));
  } else if (btnValue === "AC") {
    output = "";
  } else if (btnValue === "DEL") {
    // Si se selecciona el boton 'DEL' se eliminara el ultimo caracter de la salida
    output = output.toString().slice(0, -1);
  } else {
    // Si la salida esta vacia y botton es specialChars, vuelve al return
    if (output === "" && specialChars.includes(btnValue)) return;
    output += btnValue;
  }
  display.value = output;
};

// Se agrega el detector de evento a los botones de la calculadora, se llama a calcular() al seleccionar
buttons.forEach((button) => {
  // El valor del conjunto de datos se usa como argumento al seleccionar calcular()
  button.addEventListener("click", (e) => calculate(e.target.dataset.value));
});
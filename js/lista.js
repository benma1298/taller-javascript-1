// Se declaran las variables
const formulario = document.querySelector("#formulario");
const tareas = document.querySelector("#tareas");
const total = document.querySelector("#total");
const completadas = document.querySelector("#completadas");
let task = [];

// Se declaran los eventos
(() => {
    formulario.addEventListener('submit', validarFormulario);
    tareas.addEventListener("click", eliminarTarea);
    tareas.addEventListener("click", completarTarea);
    document.addEventListener("DOMContentLoaded", () => {
        let datosLS = JSON.parse(localStorage.getItem("tareas")) || [];
        task = datosLS;
        agregarHTML();
    })
})()

// Funcion para validar el formulario al enviar
function validarFormulario(e) {
    e.preventDefault();
    // Se obtiene el valor del campo "tarea"
    const tarea = document.querySelector("#tarea").value;
    if (tarea.trim().length === 0) {
        console.log('vacio');
        return
    }
    // Se crea el objeto tarea
    const objTarea = { id: Date.now(), tarea: tarea, estado: false };
    // Se agrega al array sin modificar el arreglo
    task = [...task, objTarea];
    formulario.reset();
    // Se agrega al HTML
    agregarHTML();
}

// Funcion para agregar las tareas al HTML
function agregarHTML() {
    // Se limpia el HTML
    while (tareas.firstChild) {
        tareas.removeChild(tareas.firstChild)
    }
    if (task.length > 0) {
        // Si el arreglo task tiene elementos, se realiza lo siguiente
        task.forEach(item => {
            // Para cada elemento en el arreglo task, se crea un nuevo elemento div
            const elemento = document.createElement('div');
            elemento.classList.add('item-tarea');
            // Se establece el contenido HTML del nuevo elemento div
            elemento.innerHTML = `
                <p>${item.estado ? (
                    `<span class='completa'>${item.tarea}</span>`
                ) : (
                    `<span>${item.tarea}</span>`
                )}</p>
                <div class="botones">
                    <button class="eliminar" data-id="${item.id}">x</button>
                    <button class="completada" data-id="${item.id}">ok</button>
                </div>
            `
            // Se agrega el nuevo elemento al elemento con el id 'tareas'
            tareas.appendChild(elemento)
        });

    } else {
        // Si el arreglo task se encuentra vacio, se muestra un mensaje indicando que no hay tareas
        const mensaje = document.createElement("h5");
        mensaje.textContent = "No hay TAREAS"
        tareas.appendChild(mensaje)
    }
    // Se calcula el total de tareas y tareas completadas
    let totalTareas = task.length;
    let tareasCompletas = task.filter(item => item.estado === true).length;

    // Se actualiza el contenido de elementos en el documento HTML con los recuentos de tareas
    total.textContent = `Total tareas: ${totalTareas}`;
    completadas.textContent = `Tareas Completadas: ${tareasCompletas}`;
    // Se guarda el arreglo task en el almacenamiento local
    localStorage.setItem("tareas", JSON.stringify(task))
}
// Se crea una funcion para cuando se elimina una tarea
function eliminarTarea(e) {
    if (e.target.classList.contains("eliminar")) {
        // Se verifica si el elemento clickeado contiene la clase "eliminar"
        const tareaID = Number(e.target.getAttribute("data-id"));
        const nuevasTareas = task.filter((item) => item.id !== tareaID);
        task = nuevasTareas;
        // Se actualiza el HTML
        agregarHTML();
    }
}


// Funcion para marcar una tarea como completada
function completarTarea(e) {
    if (e.target.classList.contains("completada")) {
        // Se verifica si el elemento clickeado contiene la clase "completada"
        const tareaID = Number(e.target.getAttribute("data-id"));
        const nuevasTareas = task.map(item => {
            // Se recorre el arreglo task y crea un nuevo arreglo con las tareas actualizadas
            if (item.id === tareaID) {
                item.estado = !item.estado;
                return item;
            } else {
                return item
            }
        })

        // Se actualiza el arreglo task con el nuevo arreglo de tareas
        task = nuevasTareas;
        // Se actualiza el HTML con las tareas actualizadas
        agregarHTML();
    }
}
// Funcion que verifica los numeros ingresados por el usuario
function checkNumbers() {
  // Se obtiene el valor ingresado
  var userInput = document.getElementById("numbers").value;
  // Se divide la cadena con "," en un array de numeros
  var userNumbers = userInput.split(",");
  // Se llama a la funcion que genera los numeros sorteados
  var drawnNumbers = generateDrawnNumbers();
  // Se almacenan los numeros ingresados con los sorteados en un array
  var matchedNumbers = [];


  // Ciclo for que recorre los numeros ingresados
  for (var i = 0; i < userNumbers.length; i++) {
    var num = parseInt(userNumbers[i].trim());
    // Se verifica si el numero sorteado coincide con el numero ingresado
    if (drawnNumbers.includes(num)) {
      // Se agrega el numero que coincide al array
      matchedNumbers.push(num);
    }
  }

  var resultText;
  // Se verifica si hay numeros que coinciden con los sorteados
  if (matchedNumbers.length > 0) {
    // Existe coincidencia
    resultText = "¡Felicidades! Has ganado. Los numeros sorteados son: " + drawnNumbers.join(", ") + ". Tus numeros acertados son: " + matchedNumbers.join(", ") + ".";
  } else {
    // No existe coincidencia
    resultText = "Lo siento, no has ganado. Los numeros sorteados son: " + drawnNumbers.join(", ") + ".";
  }

  document.getElementById("result").textContent = resultText;
}


// Funcion que genera la serie de numeros sorteados al azar
function generateDrawnNumbers() {
  var numbers = [];

  // Genera numeros hasta que el array contenga 6 elementos
  while (numbers.length < 6) {

    // Se genera un numero random entre 1 y 50
    var randomNumber = Math.floor(Math.random() * 50) + 1;

    // Se verifica que el numero sorteado no este presente en el array para agregarlo a este
    if (!numbers.includes(randomNumber)) {
      numbers.push(randomNumber);
    }
  }

  // Devuelve el array con numeros sorteados
  return numbers;
}

// Se genera un numero aleatorio entre 1 y 20
var randomNumber = generateRandomNumber();

// Se obtienen los elementos HTML necesarios
var guessInput = document.getElementById("guessInput");
var message = document.getElementById("message");
var playAgainButton = document.getElementById("playAgainButton");

// Se agrega un event listener al boton "Jugar de nuevo"
playAgainButton.addEventListener("click", playAgain);

// Funcion para generar un nuevo numero aleatorio y reiniciar el juego
function playAgain() {
  randomNumber = generateRandomNumber();
  message.textContent = "";
  guessInput.disabled = false;
}


// Funcion para comprobar el numero adivinado
function checkGuess() {
  // Se obtiene el valor introducido por el usuario
  var userGuess = parseInt(guessInput.value);

  // Se comprueba si el numero ingresado por el usuario es demasiado alto, bajo o correcto
  if (userGuess < randomNumber) {
    message.textContent = "Muy bajo. ¡Intentalo de nuevo!";
  } else if (userGuess > randomNumber) {
    message.textContent = "Muy alto. ¡Intentalo de nuevo!";
  } else {
    message.textContent = "¡Correcto! Has adivinado el numero.";
    message.style.color = "green";
    guessInput.disabled = true;
  }

  // Se limpia el campo de entrada
  guessInput.value = "";
}

// Funcion para generar un numero aleatorio entre 1 y 20
function generateRandomNumber() {
  return Math.floor(Math.random() * 20) + 1;
}